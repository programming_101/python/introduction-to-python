from setuptools import setup, find_packages

DESCRIPTION = 'Introduction-to-python'
LONG_DESCRIPTION = 'A basic package to illustration.'

# Setting up
setup(
    name="intro",
    include_package_data=True,
    setup_requires=["setuptools_scm"],
    use_scm_version=True,
    author="@b64-Lorenzo (Lorenzo Zambelli)",
    author_email="<bytemath@lorenzozambelli.it>",
    description=DESCRIPTION,
    long_description_content_type="text/markdown",
    long_description=LONG_DESCRIPTION,
    packages=find_packages(),
    install_requires=[
        'numpy>=1.1', 
        'scipy>=1.1', 
        'matplotlib>=3.6'
        ],
    keywords=['python', 'scientific', 'numerical', 'introduction'],
    classifiers=[ "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
        "Operating System :: Unix",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: Microsoft :: Windows",
    ]
)