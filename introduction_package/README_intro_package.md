# Intro

Intro is a example of Python package designed for the introduction-to-python project.


## Installation

Intro is best installed in a [virtual environment](https://docs.python.org/3/library/venv.html).
We state the most common steps for creating and using a virtual environment here.
Refer to the documentation for more details.

To create a virtual environment run
```
python3 -m venv /path/to/new/virtual/environment
```

and to activate the virtual environment, run
```
source /path/to/new/virtual/environment/bin/activate
```

After this, we can install Intro from the pip package by using
```
pip install intro
```

In case the dependencies are not installed, you can run 
```
pip install -e .
```

## Build Package Locally and Installation

To build the package locally, you can use

```
python3 setup.py sdist bdist_wheel
```

Then by running 

```
pip3 install path/to/dist/intro-0.0.post0.tar.gz
```
You can installed the package in your system or virtual environment.

## Authors

* Lorenzo Zambelli [website](https://lorenzozambelli.it)