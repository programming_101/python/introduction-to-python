"""
intro: a package for the introduction to python course
========================================================

This Introductory package has been made as an example. Most of the structure
come from the ByMa packages, a numerical mathematics package made by Lorenzo Zambelli.

This part is for describing the package. 

Subpackages
-----------
Using any of these subpackages requires an explicit import. For example,
``import intro.sublibrary``.

::

 sublibrary                         --- An example of subpackage
"""

################### Checking Package Version ####################
# This codes is not necessary but it is useful when distribute 
# the package in PyP. 
import importlib as _importlib

try:
    from importlib.metadata import version  # for Python 3.8+
except ImportError:
    from pkg_resources import get_distribution, DistributionNotFound
    try:
        __version__ = get_distribution(__name__).version
    except DistributionNotFound:
        # Package is not installed
        __version__ = 'unknown'
else:
    __version__ = version(__name__)

#####################################################################


###### Exporting modules ###########
# Necessary code
submodules = [
    'sublibrary',
]

__all__ = submodules + [
    'ifthenelse', 
    'intro', 
    'sqrt1', 
    'start'
]

def __dir__():
    return __all__
####################################



## Function to check existance submodules ##################

def __getattr__(name):
    if name in submodules:
        return _importlib.import_module(f'intro.{name}')
    else:
        try:
            return globals()[name]
        except KeyError:
            raise AttributeError(
                f"Module 'intro' has no attribute '{name}'"
            )
################################################################