# Introduction to Python

In this lecture series you will learn the basic of Python. I will mainly using a platform called "Jupyter Notebooks". Jupyter notebooks are a way to combine formatted text (like the text you are reading now), Python code, and the result of your code and calculations all in one place.

## Getting started

This lecture series is structered in multiple notebooks. At the begining of each notebook there is a short abstract and learning outcomes of the notebook itself. 

You can find the topics of each notebook in the Outline of notebooks file.

In each lecture folder you find an img folder, the notebook file, and (not always) the summary file. 


## Authors and acknowledgment

This series has been made thanks to the researches stated in the references & suggestions file. 

